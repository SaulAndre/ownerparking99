# OwnerParking99 Owner Side

## Features

- Confirm booking
- Check-out parkir

## Requirements
- NodeJS 

## Setup Instruction

### 1. Install Dependencies

```sh
# Clone the example app repo
git clone https://gitlab.com/SaulAndre/ownerparking99.git
cd ownerParking99
# Install npm dependencies
npm install
```

### 2. Setup android emulator
Buka Android Studio, dan jalankan Android Emulator yang sudah diinstall

### 3. Start Mobile app
- Open 2 terminals/cmd window.
- On the first terminal, run:
```
npx react-native start
```

- In second terminal, run:
```
npx react-native run-android
```



