import React from 'react';
import {faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {View, Text, StyleSheet} from 'react-native';

export default function CustomAlert({type, title, marginBottom}) {
  if (type === 'danger') {
    return (
      <View style={[styles.wrapper, styles.danger]}>
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{color: 'white'}}
          size={24}
        />
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  } else if (type === 'success') {
    return (
      <View style={[styles.wrapper, styles.success]}>
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{color: 'white'}}
          size={24}
        />
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
  return (
    <View style={[styles.wrapper, styles.info]}>
      <FontAwesomeIcon
        icon={faExclamationCircle}
        style={{color: 'white'}}
        size={24}
      />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    display: 'flex',
    flexDirection: 'row',
    borderRadius: 5,
    alignItems: 'center',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 10,
  },
  danger: {
    backgroundColor: '#F50057',
  },
  success: {
    backgroundColor: '#66BB6A',
  },
  info: {
    backgroundColor: '#2196F3',
  },
});
