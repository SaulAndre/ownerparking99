import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {DefaultTheme, TextInput as Input} from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#2196F3',
    accent: 'yellow',
  },
};

export default function TextInput({errorText, description, ...props}) {
  return (
    <View style={styles.container}>
      <Input
        style={styles.input}
        selectionColor="#2196F3"
        underlineColor="transparent"
        mode="outlined"
        {...props}
      />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 5,
  },
  input: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#2196F3',
    borderRadius: 10,
    color: 'black',
  },
  description: {
    fontSize: 13,
    color: '#66BB6A',
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: '#F50057',
    paddingTop: 8,
  },
});
