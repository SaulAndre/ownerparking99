import {db, timestamp} from './config';
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
  deleteDoc,
} from 'firebase/firestore/lite';
import {generateId} from '../utils';

const getAllParkiranName = async () => {
  var data = [];
  const q = query(collection(db, 'parkiran'));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach(doc => {
    data.push({
      id: doc.data().id,
      name: doc.data().name,
    });
  });
  return data;
};

const getParkiran = async parkiranId => {
  var data;
  const q = query(collection(db, 'parkiran'), where('id', '==', parkiranId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
};

const getTicket = async ticketId => {
  var data;
  const q = query(collection(db, 'tiket'), where('id', '==', ticketId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (data = doc.data()));
  return data;
};

const getAllParkiranTicket = async parkiranId => {
  var data = [];
  const q = query(
    collection(db, 'tiket'),
    where('parkiranId', '==', parkiranId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data.push(doc.data());
  });
  return data;
};

const getNumbOfVehicleToday = async parkingId => {
  var data = [];
  const startOfDay = new Date();
  startOfDay.setHours(0, 0, 0, 0);
  const q = query(
    collection(db, 'tiket'),
    where('parkingId', '==', parkingId),
    where('checkIn', '>', startOfDay),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach(doc => {
    data.push(doc.data());
  });
  return data.length;
};

const getAllParkiranActiveTicket = async parkiranId => {
  var data = [];
  const q = query(
    collection(db, 'activeTicket'),
    where('parkiranId', '==', parkiranId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => data.push(doc.data()));
  return data;
};

const getUser = async userId => {
  var data;
  var q = query(collection(db, 'users'), where('id', '==', userId));
  var querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
};

const getAllParkiranBookingTicket = async parkiranId => {
  var data = [];
  const q = query(
    collection(db, 'bookingTicket'),
    where('parkiranId', '==', parkiranId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => data.push(doc.data()));
  return data;
};

const getBookingTicket = async ticketId => {
  var data;
  var q = query(
    collection(db, 'bookingTicket'),
    where('ticketId', '==', ticketId),
  );
  var querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
};

const setCheckout = async ticketId => {
  var activeTicketId;
  const q = query(
    collection(db, 'activeTicket'),
    where('ticketId', '==', ticketId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (activeTicketId = doc.data().id));
  await deleteDoc(doc(db, 'activeTicket', activeTicketId));

  await setDoc(
    doc(db, 'tiket', ticketId),
    {
      status: 'paid',
      checkOut: timestamp.now(),
    },
    {merge: true},
  ).catch(err => {
    console.log(err.code);
  });
};

const confirmBooking = async bookingId => {
  await setDoc(
    doc(db, 'bookingTicket', bookingId),
    {
      status: 'confirmed',
    },
    {merge: true},
  ).catch(err => {
    console.log(err.code);
  });
};

const deleteBooking = async bookingId => {
  await deleteDoc(doc(db, 'bookingTicket', bookingId));
};

const checkInFromBooking = async (
  ticketId,
  parkiranId,
  userId,
  plateNumber,
) => {
  var activeTicketId = parkiranId.substring(0, 5) + userId.substring(0, 5);
  await setDoc(doc(db, 'activeTicket', activeTicketId), {
    id: activeTicketId,
    ticketId: ticketId,
    parkiranId: parkiranId,
    userId: userId,
    plateNumber: plateNumber,
    checkIn: timestamp.now(),
  }).catch(err => {
    console.log(err.code);
  });
  await setDoc(
    doc(db, 'tiket', ticketId),
    {
      status: 'dp',
      checkIn: timestamp.now(),
    },
    {merge: true},
  ).catch(err => {
    console.log(err.code);
  });
};

export {
  getAllParkiranTicket,
  getAllParkiranName,
  getParkiran,
  getAllParkiranActiveTicket,
  getUser,
  getNumbOfVehicleToday,
  setCheckout,
  getTicket,
  getAllParkiranBookingTicket,
  getBookingTicket,
  confirmBooking,
  deleteBooking,
  checkInFromBooking,
};
