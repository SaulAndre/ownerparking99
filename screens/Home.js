import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import BottomNav from '../components/BottomNav';
import CircleIcon from '../components/CircleIcon';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useAuth} from '../firebase/auth';
import {
  deleteBooking,
  confirmBooking,
  getAllParkiranActiveTicket,
  getAllParkiranBookingTicket,
  getAllParkiranTicket,
  getNumbOfVehicleToday,
  getParkiran,
  getUser,
} from '../firebase/service';
import moment from 'moment';
import {threeDots} from '../utils';
import {faCheck, faExclamation} from '@fortawesome/free-solid-svg-icons';

export default function Home({navigation}) {
  const {currentUser, logout} = useAuth();
  const [parkiran, setParkiran] = useState();
  const [tickets, setTickets] = useState([]);
  const [activeTickets, setActiveTickets] = useState([]);
  const [bookingTickets, setBookingTickets] = useState([]);
  const [todayVehicle, setVehicle] = useState();
  const [reload, setReload] = useState(0);
  const [loading, setLoading] = useState(true);

  const removeActiveTicketFromTicketList = (listTickets, listActiveTickets) => {
    var intersect = [];
    if (listActiveTickets.length === 0) return listTickets;
    listTickets.forEach(ticket => {
      listActiveTickets.forEach(activeTicket => {
        if (ticket.id !== activeTicket.ticketId) intersect.push(ticket);
      });
    });
    return intersect;
  };

  const fetchData = async () => {
    await getParkiran(currentUser.uid).then(res => {
      setParkiran(res);
    });
    await getAllParkiranTicket(currentUser.uid).then(res => {
      setTickets(res);
    });
    await getAllParkiranActiveTicket(currentUser.uid).then(res => {
      setActiveTickets(res);
    });
    await getAllParkiranBookingTicket(currentUser.uid).then(res => {
      setBookingTickets(res);
    });
    // await getNumbOfVehicleToday(currentUser.uid).then(res => {
    //   setVehicle(res);
    // });
  };

  const handleUserData = async userId => {
    var user;
    await getUser(userId).then(res => {
      user = res;
    });
    return user;
  };

  const handleConfirm = bookingId => {
    console.log('confirm');
    confirmBooking(bookingId).then(() => {
      setReload(reload + 1);
    });
  };

  const handleCancel = bookingId => {
    console.log('cancel', bookingId);
    deleteBooking(bookingId).then(() => {
      setReload(reload + 1);
    });
  };

  const reloadBooking = async () => {
    setBookingTickets([]);
    await getAllParkiranBookingTicket(currentUser.uid).then(res => {
      setBookingTickets(res);
    });
  };

  // useEffect(() => {
  //   console.log('useEffect----------\n', activeTickets);
  // }, [activeTickets]);
  useEffect(() => {
    reloadBooking();
  }, [reload]);

  useEffect(() => {
    if (tickets.length !== 0 && activeTickets !== 0) {
      setTickets(removeActiveTicketFromTicketList(tickets, activeTickets));
    }
  }, [activeTickets]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
      fetchData().then(() => setLoading(false));
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    fetchData().then(() => {
      setLoading(false);
    });
  }, []);

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.header}>
          <Text
            style={{
              fontSize: 24,
              fontWeight: 'bold',
              color: 'white',
              marginBottom: 10,
            }}>
            {parkiran ? parkiran.name : null}
          </Text>
          <View style={styles.stats}>
            <View style={styles.statsItem}>
              <Text style={{fontSize: 16, marginBottom: 5}}>
                Kendaraan Parkir
              </Text>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'right',
                }}>
                {activeTickets.length}
              </Text>
            </View>
            {/* <View style={styles.statsItem}>
              <Text style={{fontSize: 16, marginBottom: 5}}>
                Total Parkir Hari Ini
              </Text>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'right',
                }}>
                {todayVehicle}
              </Text>
            </View> */}
          </View>
        </View>
        <View style={styles.content}>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Booking List
          </Text>
          {bookingTickets.length !== 0 ? (
            <ScrollView horizontal={true} style={styles.bookingItemWrapper}>
              {bookingTickets.map(ticket => {
                if (ticket.status === 'confirmed') {
                  return (
                    <TouchableWithoutFeedback
                      key={ticket.ticketId}
                      onPress={() => {
                        console.log('pressed');
                      }}>
                      <View style={styles.bookingItem}>
                        <View style={{marginRight: 15}}>
                          <Text style={{color: 'black'}}>
                            Perkiraan Check-in
                          </Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#2196F3',
                              marginBottom: 5,
                            }}>
                            {moment(ticket.estCheckIn.toDate()).format('lll')}
                          </Text>
                          <Text style={{color: 'black'}}>Nomor Kendaraan</Text>
                          <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                            {ticket.plateNumber}
                          </Text>
                        </View>
                        <View
                          style={{
                            width: 100,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <CircleIcon
                            icon={faCheck}
                            bgColor="#66BB6A"
                            iconColor="white"
                            size={40}
                            iconSize={20}
                          />
                          <Text
                            style={{
                              color: 'black',
                              textAlign: 'center',
                              marginTop: 5,
                            }}>
                            Booking berhasil
                          </Text>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  );
                }
                return (
                  <TouchableWithoutFeedback key={ticket.ticketId}>
                    <View style={styles.bookingItem}>
                      <View style={{marginRight: 15}}>
                        <Text style={{color: 'black'}}>Perkiraan Check-in</Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#2196F3',
                            marginBottom: 5,
                          }}>
                          {moment(ticket.estCheckIn.toDate()).format('lll')}
                        </Text>
                        <Text style={{color: 'black'}}>Nomor Kendaraan</Text>
                        <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                          {ticket.plateNumber}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: 100,
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <View
                          style={{
                            paddingHorizontal: 5,
                            width: '100%',
                            marginBottom: 5,
                          }}>
                          <Button
                            title="Confirm"
                            style={{marginBottom: 100}}
                            onPress={() => handleConfirm(ticket.id)}
                          />
                        </View>
                        <View style={{paddingHorizontal: 5, width: '100%'}}>
                          <Button
                            title="Cancel"
                            color="#F50057"
                            onPress={() => handleCancel(ticket.id)}
                          />
                        </View>
                      </View>
                      {/* <View style={{display: 'flex', flexDirection: 'row'}}>
                    </View> */}
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </ScrollView>
          ) : (
            <View style={styles.activeTicket}>
              <Text style={{color: '#d4d4d4', textAlign: 'center'}}>
                Tidak ada tiket booking.
              </Text>
            </View>
          )}

          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Kendaraan Parkir Saat Ini
          </Text>
          {activeTickets.length !== 0 ? (
            activeTickets.map(ticket => {
              return (
                <View style={styles.activeTicket} key={ticket.id}>
                  <View style={{display: 'flex', flexWrap: 'wrap'}}>
                    <Text style={{color: 'black'}}>
                      Id Tiket:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {threeDots(ticket.ticketId, 12)}
                      </Text>
                    </Text>
                    <Text style={{color: 'black', fontSize: 14}}>
                      Check-in:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {moment(ticket.checkIn.toDate()).format('lll')}
                      </Text>
                    </Text>
                  </View>
                  <View
                    style={{
                      width: 170,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 14, color: 'black'}}>
                      Nomer Kendaraan
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#2196F3',
                        fontSize: 28,
                      }}>
                      {ticket.plateNumber}
                    </Text>
                  </View>
                </View>
              );
            })
          ) : (
            <View style={styles.activeTicket}>
              <Text style={{color: '#d4d4d4', textAlign: 'center'}}>
                Tidak ada kendaraan yang sedang parkir
              </Text>
            </View>
          )}
          <View style={styles.allTicket}>
            <Text
              style={{
                fontSize: 18,
                color: 'black',
                fontWeight: 'bold',
                marginBottom: 10,
              }}>
              History Parkir
            </Text>
            {tickets
              ? tickets.map(ticket => {
                  if (ticket.status === 'booking') return null;
                  return (
                    <TouchableWithoutFeedback
                      key={ticket.id}
                      onPress={() =>
                        navigation.navigate('Checkout', {ticketId: ticket.id})
                      }>
                      <View style={styles.activeTicket} key={ticket.id}>
                        <View style={{display: 'flex', flexWrap: 'wrap'}}>
                          <Text style={{color: 'black'}}>
                            Id Tiket:&nbsp;
                            <Text
                              style={{fontWeight: 'bold', color: '#2196F3'}}>
                              {threeDots(ticket.id, 12)}
                            </Text>
                          </Text>
                          <Text style={{color: 'black', fontSize: 14}}>
                            Check-in:&nbsp;
                            <Text
                              style={{fontWeight: 'bold', color: '#2196F3'}}>
                              {moment(ticket.checkIn.toDate()).format('lll')}
                            </Text>
                          </Text>
                        </View>
                        <View
                          style={{
                            width: 170,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text style={{fontSize: 14, color: 'black'}}>
                            Nomer Kendaraan
                          </Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#2196F3',
                              fontSize: 28,
                            }}>
                            {ticket.plateNumber}
                          </Text>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  );
                })
              : null}
          </View>
        </View>
      </ScrollView>
      <BottomNav navigation={navigation} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    backgroundColor: '#2196F3',
    padding: 15,
    paddingVertical: 25,
    paddingTop: 15,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  stats: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  statsItem: {
    marginRight: 10,
  },
  content: {
    padding: 20,
  },
  bookingItemWrapper: {
    overflowHorizontal: 'scroll',
    whiteSpace: 'nowrap',
    display: 'flex',
    flexDirection: 'row',
    boxSizing: 'border-box',
    marginBottom: 10,
  },
  bookingItem: {
    padding: 15,
    backgroundColor: 'white',
    marginRight: 10,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  activeTicket: {
    marginBottom: 10,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  activeTicketDetail: {
    marginLeft: 15,
  },
});
