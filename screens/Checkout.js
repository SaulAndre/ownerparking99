import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Button, ScrollView} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import CustomAlert from '../components/CustomAlert';
import DetailTicket from '../components/DetailTicket';
import {getTicket, setCheckout} from '../firebase/service';

export default function Checkout({navigation, route}) {
  const [loading, setLoading] = useState(true);
  const [ticket, setTicket] = useState(null);
  const [reloadPurpose, setReload] = useState(1);
  const {ticketId, parkiranId} = route.params;

  const handleCheckout = async () => {
    await setCheckout(ticketId);
    setReload(reloadPurpose + 1);
  };

  const fetchData = () => {
    getTicket(ticketId)
      .then(res => {
        setTicket(res);
        console.log(res);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchData();
    console.log('reload---------------\n', reloadPurpose);
  }, [reloadPurpose]);

  if (loading) return <ActivityIndicator size="large" color="#2196F3" />;
  else if (ticket.status === 'paid') {
    return (
      <ScrollView style={styles.ticketWrapper}>
        <CustomAlert title="Tiket Sudah Dibayar" type="success" />
        <DetailTicket ticketId={ticketId} borderColor={'#66BB6A'} calculate />
        <View style={{marginBottom: 50}}>
          <Button
            title="Kembali Ke Beranda"
            onPress={() => navigation.navigate('Home')}
          />
        </View>
      </ScrollView>
    );
  } else if (ticket.status === 'dp') {
    return (
      <ScrollView style={styles.ticketWrapper}>
        <CustomAlert title="Tiket Butuh Penyelesaian" type="danger" />
        <DetailTicket ticketId={ticketId} borderColor={'#F50057'} booking />
        <View style={{marginBottom: 50}}>
          <Button
            title="Konfirmasi Pembayaran"
            onPress={() => {
              handleCheckout();
            }}
          />
        </View>
      </ScrollView>
    );
  }
  return (
    <ScrollView style={styles.ticketWrapper}>
      <CustomAlert title="Tiket Belum Dibayar" type="danger" />
      <DetailTicket ticketId={ticketId} borderColor={'#F50057'} calculate />
      <View style={{marginBottom: 50}}>
        <Button
          title="Konfirmasi Pembayaran"
          onPress={() => {
            handleCheckout();
          }}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  ticketWrapper: {
    padding: 30,
    paddingBottom: 100,
  },
});
